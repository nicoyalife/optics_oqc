#!/usr/bin/python3
import os
import time
import sys
import subprocess
import signal

from common.alto_core import AltoCore
from common.print_colours import *

engage = True
loop = False

# Constantsgmai
DOOR = "DOOR"
SHUTTLE = "SHUTTLE"
ZLIFT = "ZLIFT"
FCI = "FCI"
# Motor targets
DOOR_TARGET = "DR"
SHUTTLE_TARGET = "SH"
ZLIFT_TARGET = "ZL"
FCI_TARGET = "FC"
# Motor command
MOTOR_LIMIT = "POS_____"
# Motor limit position
HOME = "0"
END = "1"
# Motor Responses
HOME_RESP = "HOMED"
END_RESP = "ENDED"

# used in start and kill functions
ir_camera_proc = None

# Get the motor target from the string ID
def get_motor_target_from_string_id(string_id):
    if string_id == DOOR:
        return DOOR_TARGET
    elif string_id == SHUTTLE:
        return SHUTTLE_TARGET
    elif string_id == ZLIFT:
        return ZLIFT_TARGET
    elif string_id == FCI:
        return FCI_TARGET

def start_ir_camera():
    global ir_camera_proc
    gst_ir_camera_cmd = "gst-launch-1.0 v4l2src device=\"/dev/video0\" ! video/x-raw,framerate=15/1,width=640,height=360 ! videoconvert ! autovideosink"
    ir_camera_proc = subprocess.Popen(gst_ir_camera_cmd, shell=True, preexec_fn=os.setsid)

def kill_ir_camera():
    # kill the camera process
    global ir_camera_proc
    os.killpg(os.getpgid(ir_camera_proc.pid), signal.SIGTERM)

# Test ejecting cartridge (All END)
def eject_cartridge(alto_core):
    print_info("Unloading cartridge...")
    # Success flag
    success = True

    # build commands for motors
    FCI_CMD = get_motor_target_from_string_id(FCI) + MOTOR_LIMIT + "1" + HOME

    ZLIFT_CMD = get_motor_target_from_string_id(ZLIFT) + MOTOR_LIMIT + "1" + HOME
    SHUTTLE_CMD = get_motor_target_from_string_id(SHUTTLE) + MOTOR_LIMIT + "1" + HOME
    DOOR_CMD = get_motor_target_from_string_id(DOOR) + MOTOR_LIMIT + "1" + HOME

    # if the hv board is connected disable it
    if alto_core.hv_ser != None:
        alto_core.enable_hv_sic(enable = False)

    # execute the motor commands
    print_info("Sending FCI to HOME")
    position = alto_core.send_tm_serial(FCI_CMD) 
    if position != HOME_RESP:
        success = False
    print_info("Sending Z-LIFT to HOME")
    position = alto_core.send_tm_serial(ZLIFT_CMD)
    if position != HOME_RESP:
        success = False
    print_info("Sending SHUTTLE to HOME")
    position = alto_core.send_tm_serial(SHUTTLE_CMD)
    if position != HOME_RESP:
        success = False
    print_info("Sending DOOR to HOME")
    position = alto_core.send_tm_serial(DOOR_CMD)
    if position != HOME_RESP:
        success = False

    # return the success flag
    return success


# Test loading cartridge (All HOME)
def load_cartridge(alto_core):
    print_info("Loading cartridge...")
    # Success flag
    success = True

    # build commands for motors
    DOOR_CMD = get_motor_target_from_string_id(DOOR) + MOTOR_LIMIT + "1" + END
    # DOOR_Limit over
    SHUTTLE_CMD = get_motor_target_from_string_id(SHUTTLE) + MOTOR_LIMIT + "1" + END
    ZLIFT_CMD = get_motor_target_from_string_id(ZLIFT) + MOTOR_LIMIT + "1" + END
    FCI_CMD = get_motor_target_from_string_id(FCI) + MOTOR_LIMIT + "1" + END

    # execute the motor commands
    print_info("Sending DOOR to END")
    position = alto_core.send_tm_serial(DOOR_CMD)
    if position != END_RESP:
        success = False
    print_info("Sending SHUTTLE to END")
    position = alto_core.send_tm_serial(SHUTTLE_CMD)    
    if position != END_RESP:
        success = False
    # position = alto_core.send_tm_serial(get_motor_target_from_string_id(SHUTTLE) + "STEP____2-28") 
    print_info("Sending Z-LIFT to END")
    position = alto_core.send_tm_serial(ZLIFT_CMD)    
    if position != END_RESP:
        success = False    
    position = alto_core.send_tm_serial(get_motor_target_from_string_id(ZLIFT) + "STEP____48000") 
    print_info("Sending FCI to END")
    position = alto_core.send_tm_serial(FCI_CMD)  
    if position != END_RESP:
        success = False

    # if the hv board is connected enable it
    if alto_core.hv_ser != None:
        alto_core.enable_hv_sic(enable = True)

    # return the success flag
    return success

    # kill camera stream
    kill_ir_camera()

    return success

if __name__ == '__main__':
    # Get the header text
    header_text = "Disengage"
    if engage == True:
        header_text = "Engage"
    else:
        header_text = "Disengage"

    if loop == True:
        header_text = "loop"
        
    try:
        success = True
        # clear terminal
        os.system('cls' if os.name == 'nt' else 'clear')

        # Print test header
        print_info("===========================================================")
        print_info(header_text + " Cartridge")
        print_info("===========================================================")
    
        # Create a new alto core object
        alto_core = AltoCore()

        if loop == True:
            count = 0
            while True == True:
                load_cartridge(alto_core = alto_core)
                eject_cartridge(alto_core = alto_core)
                count += 1
                print_info("Loop count = " + str(count))

        # Perform action
        if engage == True:
            success = load_cartridge(alto_core = alto_core)
        else:
            success = eject_cartridge(alto_core = alto_core)

        if success == True:                
            # Write success message
            print_pass("===========================================================")
            print_pass(header_text + " Cartridge PASSED")
            print_pass("===========================================================")
    except Exception as e:
        print_fail("===========================================================")
        print_fail(header_text + " Cartridge FAILED")
        print_fail("Error = " + str(e))
        print_fail("===========================================================")     