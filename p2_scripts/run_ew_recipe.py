#!/usr/bin/python3
import sys
import time
import os
from common.alto_core import AltoCore

# Fluidic recipe
EW_TEST_RECIPE = "/config/test_nico_file.nico"

# Mux Header
MUX_HEADER = "HVMUX_____X"

# Known commands
MUX = "MUX"
WAIT = "WAIT"

class PrintColours():
    COLOUR_DEFAULT = "\033[39m"
    COLOUR_FAIL = "\033[31m"
    COLOUR_PASS = "\033[32m"
    COLOUR_INFO = "\033[34m"   

def print_info(text):
    print(PrintColours.COLOUR_INFO + text + PrintColours.COLOUR_DEFAULT)

def print_fail(text):
    print(PrintColours.COLOUR_FAIL + text + PrintColours.COLOUR_DEFAULT)

def print_pass(text):
    print(PrintColours.COLOUR_PASS + text + PrintColours.COLOUR_DEFAULT)

def progressBar(current, total, barLength = 20):
    percent = float(current) * 100 / total
    arrow   = '-' * int(percent/100 * barLength - 1) + '>'
    spaces  = ' ' * (barLength - len(arrow))

    print('Progress: [%s%s] %d %%' % (arrow, spaces, percent), end='\r')

def execute_cmd(alto_core, cmd):
    try:
        # Split the cmd
        split = cmd.split(" ")
        # if cmd has no payload return
        if len(split) < 2:
            return
        # get the header and payload
        cmd_header = split[0]
        cmd_payload = split[1].strip("\n").strip("\r")

        # Find what the command header is
        if cmd_header == WAIT:
            # Is a wait command
            time.sleep(float(cmd_payload) / 1000)
        elif cmd_header == MUX:
            # is a mux command
            cmd_payload = cmd_payload[2:]       # remove the 0x
            resp = alto_core.send_hv_serial(MUX_HEADER + cmd_payload)
            if resp != cmd_header + " = " + cmd_payload:
                raise Exception("Invalid mux response.")
    except Exception as e:
        raise Exception(e)
    

def run_ew_recipe(alto_core, recipe_file):
    try:
        # Success flag
        success = True

        # get the number of lines in file
        num_lines = sum(1 for line in open(recipe_file))

        # execution counter
        counter = 0

        # loop the file
        with open(recipe_file) as ew_file:
            # loop line by line
            for line in ew_file:
                # execute the line
                execute_cmd(alto_core = alto_core, cmd = line)
                
                # iterate the counter
                counter += 1

                # print percentage complete
                percentage = counter / num_lines * 100
                progressBar(current = percentage, total = 100)

        # Return the success flag
        return success
    except Exception as e:
        raise Exception("Error run_ew_recipe(): " + str(e))

if __name__ == '__main__':
    try:
        success = True

        # Get working directory 
        dir_path = os.path.dirname(os.path.realpath(__file__))

        # clear terminal
        os.system('cls' if os.name == 'nt' else 'clear')

        # Print test header
        print_info("===========================================================")
        print_info("EW Recipe Test")
        print_info("===========================================================")

        # Create a new alto core object
        alto_core = AltoCore()

        # Run the ew recipe
        success = run_ew_recipe(alto_core = alto_core, recipe_file = dir_path + EW_TEST_RECIPE)

        if success == True:
            # Write success message
            print_pass("===========================================================")
            print_pass("EW Recipe Test PASSED")
            print_pass("===========================================================")

    except Exception as e:
        print_fail("===========================================================")
        print_fail("EW Recipe Test FAILED")
        print_fail("Error = " + str(e))
        print_fail("===========================================================")