#!/usr/bin/python3
# pyftdi libs
from pyftdi.ftdi import Ftdi
from pyftdi.usbtools import UsbTools
import pyftdi.serialext
from pyftdi.gpio import GpioController, GpioException, GpioAsyncController
from pyftdi.spi import SpiController
from pyftdi.i2c import I2cController

# python libs
import sys
import os
from collections import OrderedDict
import time


PACKAGE_PARENT = '..'
SCRIPT_DIR = os.path.dirname(os.path.realpath(os.path.join(os.getcwd(), os.path.expanduser(__file__))))
sys.path.append(os.path.normpath(os.path.join(SCRIPT_DIR, PACKAGE_PARENT)))

from common.print_colours import *

SCHEME = 'ftdi'
"""URL scheme for :py:class:`UsbTools`."""

FTDI_VENDOR = 0x403
"""USB VID for FTDI chips."""

VENDOR_IDS = {'ftdi': FTDI_VENDOR}
"""Supported vendors, only FTDI.
    To add third parties vendors see :py:meth:`add_custom_vendor`.
"""

PRODUCT_IDS = {
    FTDI_VENDOR: OrderedDict((
        # use an ordered dict so that the first occurence of a PID takes
        # precedence when generating URLs - order does matter.
        ('232', 0x6001),
        ('232r', 0x6001),
        ('232h', 0x6014),
        ('2232', 0x6010),
        ('2232c', 0x6010),
        ('2232d', 0x6010),
        ('2232h', 0x6010),
        ('4232', 0x6011),
        ('4232h', 0x6011),
        ('ft-x', 0x6015),
        ('230x', 0x6015),
        ('231x', 0x6015),
        ('234x', 0x6015),
        ('ft232', 0x6001),
        ('ft232r', 0x6001),
        ('ft232h', 0x6014),
        ('ft2232', 0x6010),
        ('ft2232c', 0x6010),
        ('ft2232d', 0x6010),
        ('ft2232h', 0x6010),
        ('ft4232', 0x6011),
        ('ft4232h', 0x6011),
        ('ft230x', 0x6015),
        ('ft231x', 0x6015),
        ('ft234x', 0x6015)))
    }
"""Supported products, only FTDI officials ones.
    To add third parties and customized products, see
    :py:meth:`add_custom_product`.
"""

DEFAULT_VENDOR = FTDI_VENDOR
"""Default vendor: FTDI."""

# Baud rate
BAUD_RATE = 921600
SERIAL_TIMEOUT = 1200

# Expected FTDI devices length
EXPECTED_FTDI_LENGTH = 8
EXPECTED_FTDI_URL_INEX = 0
PERIPHERAL_FTDI_URL_INEX = 1

# Expected device indexes
EXPECTED_ID_INPUT = 1
EXPECTED_HV_INDEX = 0
EXPECTED_TM_INDEX = 1
SPI_CHANNEL_INDEX               = 0 + 4     # expected to be second FTDI
I2C_CHANNEL_INDEX               = 1 + 4     # expected to be second FTDI
EXPECTED_SIC0_EN_RES_BOOT_INDEX = 2 + 4     # expected to be second FTDI
EXPECTED_SIC1_EN_RES_BOOT_INDEX = 2 + 4     # expected to be second FTDI
EXPECTED_SIC2_EN_RES_BOOT_INDEX = 3 + 4     # expected to be second FTDI


# FTDI gpio settings on ch3 of second ftdi (with hsi and lma enables)
# Bit values
HV_EN = 0x01
HV_BOOT = 0x02
HV_RESET = 0x04
LMC_EN = 0x08
TM_EN = 0x10
TM_BOOT = 0x20
TM_RESET = 0x40
USB_HSI_EN = 0x80
# Default mask for FTDI CH3 for SIC0 and SIC1 enable
DEFAULT_FTDI_CH3_MASK = HV_RESET | LMC_EN | TM_RESET | USB_HSI_EN

# FTDI gpio settings on ch4 of second ftdi (with hsi and lma enables)
# Bit values
USB2_COMe_EN = 0x80
DEBUG_LED0 = 0x10
DEBUG_LED1 = 0x20
DEBUG_LED2 = 0x40
LASER_ENA = 0x80

# Default mask for FTDI CH4 of second ftdi
DEFAULT_FTDI_CH4_MASK = USB2_COMe_EN | DEBUG_LED0 | DEBUG_LED1 | DEBUG_LED2 | LASER_ENA

ENABLE_RST_HV_TM = 0xBB 
SINK_RST_HV_TM = 0x99

# Expected who IDs
CMD_HV_WHO = "HVWHO?____0"
EXPECTED_HV_WHO_RESP = "HV"
CMD_TM_WHO = "TMWHO?____0"
EXPECTED_TM_WHO_RESP = "THERMOTOR"

UART_FLUSH_STABILITY_DELAY = 0.25

# Intake fans cmd
CMD_INTAKE_FAN_LEFT = "FN3PWM____295"
CMD_INTAKE_FAN_RITHG = "FN4PWM____295"


def __get_connected_devices__():
    devdescs = UsbTools.list_devices('ftdi:///?',
                                     VENDOR_IDS, PRODUCT_IDS,DEFAULT_VENDOR)
    devstrs = UsbTools.build_dev_strings('ftdi', VENDOR_IDS, PRODUCT_IDS, devdescs)
    # Return the connected devices
    return devstrs

class AltoCore():
    def __init__(self):
        try:
            self.hv_ser = None
            self.tm_ser = None
            self.ftdi_gpio_en_ch3_current_mask = 0x00
            
            # Get the number of connected devices
            self.connected_devices = __get_connected_devices__()

            # Check to make sure there are connected devices
            if len(self.connected_devices) < EXPECTED_FTDI_LENGTH:
                raise Exception("Less than the expected count of FTDI devices found. There were " + str(len(self.connected_devices)) + "devices found.")
            if len(self.connected_devices) > EXPECTED_FTDI_LENGTH:
                raise Exception("Less than the expected count of FTDI devices found. There were " + str(len(self.connected_devices)) + "devices found.")

            # Turn on the light bar to solid colour
            self.turn_lightbar_all_on(r = 0, g = 204, b = 102)            
            self.turn_lightbar_all_on(r = 178, g = 102, b = 255)  
            self.turn_lightbar_all_on(r = 255, g = 255, b = 0)  # yellow
            self.turn_lightbar_all_on(r = 153, g = 0, b = 204)  # pink
            self.turn_lightbar_all_on(r = 0, g = 0, b = 0)  # off
            self.turn_lightbar_all_on(r = 0, g = 51, b = 204) # light blue

            # Turn on HSI illlumination
            # # Turn on front and back IR illumination leds
            # self.turn_on_IR_illumination_leds()

            # Set default HV and TM boards to both disabled
            self.ftdi_gpio_en_ch3_handle = GpioAsyncController()
            self.ftdi_gpio_en_ch3_handle.configure(self.connected_devices[EXPECTED_SIC0_EN_RES_BOOT_INDEX][EXPECTED_FTDI_URL_INEX], direction=0xFF)
            self.ftdi_gpio_en_ch3_handle.write(0x00)

            # ch4 on peripheral ftdi has the LASER_ENA gpio to turn on off the DAC to the HSI LEDs
            self.ftdi_gpio_en_ch4_handle = GpioAsyncController()
            self.ftdi_gpio_en_ch4_handle.configure(self.connected_devices[EXPECTED_SIC2_EN_RES_BOOT_INDEX][EXPECTED_FTDI_URL_INEX], direction=0xFF)
            self.ftdi_gpio_en_ch4_handle.write(0x00)

            time.sleep(1)
            # Update the current mask with the default value
            self.ftdi_gpio_en_ch3_handle.write(DEFAULT_FTDI_CH3_MASK)
            self.ftdi_gpio_en_ch3_current_mask = DEFAULT_FTDI_CH3_MASK

            # Update the current mask with the default value
            self.ftdi_gpio_en_ch4_handle.write(DEFAULT_FTDI_CH4_MASK)
            self.ftdi_gpio_en_ch4_current_mask = DEFAULT_FTDI_CH4_MASK

            # Enable the hv board
            self.enable_hv_sic(enable = True)
            # Enable the thermotor board
            self.enable_tm_sic(enable = True)
 
            # print the connected devices info
            print_info("Connected devices found = " + str(self.connected_devices))

            ## For debugging purposes quiet mode
            #self.quiet_mode()
            # Turn on 2x system fans
            self.turn_on_system_fan()
        except Exception as e:
            print_fail("AltoCore __init__(): " + str(e))
            raise Exception("AltoCore __init__(): " + str(e))

    # This method will send serial messages to the hv board
    def send_hv_serial(self, msg):
        try:
            # flush input/output
            self.hv_ser.flushInput()
            self.hv_ser.flushOutput()

            self.hv_ser.reset_input_buffer()
            self.hv_ser.reset_output_buffer()

            self.hv_ser.timeout = 1
            # Flush stability sleep required
            time.sleep(UART_FLUSH_STABILITY_DELAY)

            # write message
            self.hv_ser.write(msg.encode())
            self.hv_ser.write(b'\r\n')
            
            ser_buf = []
            while True:
                ser_buf.append(self.hv_ser.read(1))
                if len(ser_buf) > 3:
                    if ser_buf[len(ser_buf) - 1] == b'\x00' and ser_buf[len(ser_buf) - 2] == b'\n' and ser_buf[len(ser_buf) - 3] == b'\r':
                        break

            # read response
            resp = ""
            for i in range(0, len(ser_buf)):
                resp = resp + ser_buf[i].decode("utf-8")

            if len(resp) < 1:
                raise Exception("No response found.")
            
            # Return the response
            return resp.replace("\n","").replace("\r","").replace("\x00","").strip()
        except Exception as e:
            print_fail("AltoCore send_hv_serial(): " + string(e))
            raise Exception("AltoCore send_hv_serial(): " + string(e))

    # This method will send serial messages to the tm board
    def send_tm_serial(self, msg):
        try:
            # flush input/output
            self.tm_ser.flushInput()
            self.tm_ser.flushOutput()
            
            self.tm_ser.reset_input_buffer()
            self.tm_ser.reset_output_buffer()

            self.tm_ser.timeout = 1
            # Flush stability sleep required
            time.sleep(UART_FLUSH_STABILITY_DELAY)

            # write message
            self.tm_ser.write(msg.encode())
            self.tm_ser.write(b'\r\n')

            ser_buf = []
            while True:
                ser_buf.append(self.tm_ser.read(1))
                if len(ser_buf) > 3:
                    if ser_buf[len(ser_buf) - 1] == b'\x00' and ser_buf[len(ser_buf) - 2] == b'\n' and ser_buf[len(ser_buf) - 3] == b'\r':
                        break

            # read response
            resp = ""
            for i in range(0, len(ser_buf)):
                resp = resp + ser_buf[i].decode("utf-8")

            if len(resp) < 1:
                raise Exception("No response found.")
            
            # Return the response
            return resp.replace("\n","").replace("\r","").replace("\x00","").strip()
        except Exception as e:
            print_fail("AltoCore send_tm_serial(): " + string(e))
            raise Exception("AltoCore send_tm_serial(): " + string(e))

    # The method will enable the hv sic
    def enable_hv_sic(self, enable):    
        try:
            # Get the current mask of the ftdi gpio
            current_mask = self.ftdi_gpio_en_ch3_current_mask

            # close serial port if disabling the hv
            if enable == False:
                self.hv_ser.close()            

            # minipulate the masked result based on enable req
            if enable == True:
                # Enable HV ENA
                current_mask |= HV_EN
                # Ensure reset is low
                current_mask &= ~HV_RESET
            else:
                # Disable HV ENA
                current_mask &= ~HV_EN
                # Ensure reset is low
                current_mask &= ~HV_RESET         

            # write the new value
            self.ftdi_gpio_en_ch3_handle.write(current_mask)

            # update the current mask value
            self.ftdi_gpio_en_ch3_current_mask = current_mask

            # Stability sleep
            time.sleep(2)

            # Open the serial port
            if enable == True:
                try:
                    # Open a serial port on the hv index
                    port_url = self.connected_devices[EXPECTED_HV_INDEX][EXPECTED_FTDI_URL_INEX]

                    # If port is already open close it
                    if self.hv_ser != None:
                        self.hv_ser.close()
                        self.hv_ser = None

                    # Open new serial port
                    self.hv_ser = pyftdi.serialext.serial_for_url(port_url, baudrate = BAUD_RATE, timeout = SERIAL_TIMEOUT)

                    # Request who ID to the HV
                    resp = self.send_hv_serial(CMD_HV_WHO)

                    if resp != EXPECTED_HV_WHO_RESP:
                        raise Exception("Invalid HV who ID response found: " + str(resp))
                except Exception as ex:
                    raise Exception("Unable to open connection to HV board. " + str(ex))
                    self.hv_ser = None
        except Exception as e:
            raise Exception("enable_hv_sic(): " + str(e))

    def enable_tm_sic(self, enable):    
        try:
            # Get the current mask of the ftdi gpio
            current_mask = self.ftdi_gpio_en_ch3_current_mask

            # close serial port if disabling the tm
            if enable == False:
                self.tm_ser.close()    

            # minipulate the masked result based on enable req
            if enable == True:
                # Enable HV ENA
                current_mask |= TM_EN
                # Ensure reset is low
                current_mask &= ~TM_RESET
            else:
                # Disable HV ENA
                current_mask &= ~TM_EN
                # Ensure reset is low
                current_mask &= ~TM_RESET                  

            # write the new value
            self.ftdi_gpio_en_ch3_handle.write(current_mask)

            # update the current mask value
            self.ftdi_gpio_en_ch3_current_mask = current_mask

            # Stability sleep for power up
            time.sleep(5)

            # Open the serial port if enable requested
            if enable == True:
                # Connect to the tm board
                try:
                    # Open a serial port on the tm index
                    port_url = self.connected_devices[EXPECTED_TM_INDEX][EXPECTED_FTDI_URL_INEX]
                    self.tm_ser = pyftdi.serialext.serial_for_url(port_url, baudrate = BAUD_RATE, timeout = SERIAL_TIMEOUT)

                    # Request who ID to the TM
                    resp = self.send_tm_serial(CMD_TM_WHO)

                    if resp != EXPECTED_TM_WHO_RESP:
                        raise Exception("Invalid Thermotor who ID response found: " + str(resp))

                except Exception as ex:
                    print_fail("Unable to open connection to Thermotor board. " + str(ex))
                    self.tm_ser = None
        except Exception as e:
            raise Exception("enable_hv_sic(): " + str(e))        


    # turn on all leds WHITE for EMC test
    def turn_lightbar_all_on(self, r, g, b):
        try:
            # Sanity check the rgb values
            if r > 255 or r < 0:
                raise Exception("Invalid r value must be between 0-255.")
            if g > 255 or g < 0:
                raise Exception("Invalid g value must be between 0-255.")
            if b > 255 or g < 0:
                raise Exception("Invalid b value must be between 0-255.")

            # Convert the rgb values
            r_val = r.to_bytes(length = 1, byteorder = 'big')
            g_val = g.to_bytes(length = 1, byteorder = 'big')
            b_val = b.to_bytes(length = 1, byteorder = 'big')

            # Set up the spi communication
            spi = SpiController(cs_count=5)
            port_url = self.connected_devices[SPI_CHANNEL_INDEX][EXPECTED_FTDI_URL_INEX]
            spi.configure(port_url)
            slave = spi.get_port(cs=0, freq=500000, mode=2)

            data_byte_array = []
            brightness = b'\xFF'        # For now use max brightness

            # build the start bytes
            for i in range(0, 4):
                data_byte_array.append(b'\x00')

            # build the data bytes
            for i in range(0, 28):
                data_byte_array.append(brightness)
                data_byte_array.append(b_val)
                data_byte_array.append(g_val)
                data_byte_array.append(r_val)

            # build the end frame
            for i in range(0, 14):
                data_byte_array.append(b'\x00')     

            # join the bytes
            joined_bytes = b''.join(data_byte_array)

            # write the data to the spi
            slave.write(joined_bytes)       
        except Exception as e:
            raise Exception("Error turn_lightbar_all_on(): " + str(e))

    # turn on all leds WHITE for EMC test
    def turn_on_hsi_illumination(self, on):
        
        spi = SpiController(cs_count=5)
        port_url = self.connected_devices[SPI_CHANNEL_INDEX][EXPECTED_FTDI_URL_INEX]
        spi.configure(port_url)
        slave = spi.get_port(cs=4, freq=5000, mode=2)

        write_buf = b''
        if on:
            # re-enable the LASER_ENA
            current_mask = self.ftdi_gpio_en_ch4_current_mask
            current_mask |= LASER_ENA
            self.ftdi_gpio_en_ch4_handle.write(current_mask)
            self.ftdi_gpio_en_ch4_current_mask = current_mask

            # Set DAC output
            write_buf = b'\x22e0' #1V8  100%
            #write_buf = b'\x0117' #0V9 50%
        else:
            #write_buf = b'\x0000' #0V0 # this will only dim the leds
            # disable the LASER_ENA
            current_mask = self.ftdi_gpio_en_ch4_current_mask
            current_mask &= ~LASER_ENA
            self.ftdi_gpio_en_ch4_handle.write(current_mask)
            self.ftdi_gpio_en_ch4_current_mask = current_mask

        slave.write(write_buf)

    # turn on front and back IR illumination leds
    def turn_on_IR_illumination_leds(self):
        # Device I2C addresses
        BROADCAST = 0x47
        FRONT_ILLUMINATOR = 0x49
        BACK_ILLUMINATOR = 0x4B

        # Register addresses
        STATUS = 0xD0
        GENERAL_CONFIG = 0xD1
        DAC_DATA = 0x21
        TRIGGER = 0xD3
        DAC_MARGIN_HIGH = 0x25
        DAC_MARGIN_LOW = 0x26

        # set up i2c channel
        i2c = I2cController()
        port_url = self.connected_devices[I2C_CHANNEL_INDEX][EXPECTED_FTDI_URL_INEX]
        i2c.configure(port_url)
        front_slave = i2c.get_port(FRONT_ILLUMINATOR)
        back_slave = i2c.get_port(BACK_ILLUMINATOR)

        # write to front DAC
        front_slave.write([GENERAL_CONFIG, 0x11, 0xE5]) 
        front_slave.write([DAC_DATA,0x0B, 0x86])  # 1.8V
        front_slave.write([DAC_MARGIN_HIGH,0x0A, 0x58])
        front_slave.write([DAC_MARGIN_LOW,0x0A, 0x58])
        front_slave.write([TRIGGER,0x00, 0x10])

        # write to back DAC
        back_slave.write([GENERAL_CONFIG, 0x11, 0xE5]) 
        back_slave.write([DAC_DATA,0x0B, 0x86])  # 1.8V
        back_slave.write([DAC_MARGIN_HIGH,0x0A, 0x58])
        back_slave.write([DAC_MARGIN_LOW,0x0A, 0x58])
        back_slave.write([TRIGGER,0x00, 0x10])

    # Turn on 2x intake fans
    def turn_on_system_fan(self):
        self.send_tm_serial(CMD_INTAKE_FAN_LEFT)
        self.send_tm_serial(CMD_INTAKE_FAN_RITHG)

    def toggle_light_bar(self,op):       
        if op == "on":
            self.turn_lightbar_all_on(r = 0, g = 51, b = 204)
        elif op == "off":
            self.turn_lightbar_all_on(r = 0, g = 0, b = 0)

    # Adjust PID parameters of TEC zones
    def set_cool_tec_pid(self, pwm_value):
        """
        For now:
        - Both Sensor and Reagent zones together
        - Only setting PWM, fixed P,I,D values to 500:3000:4000
        """
        try:
            if pwm_value < 100:
                SET_SENSOR_ZONE_PID_CMD = "TSPIDPIDCL22500:3000:4000:50:" + str(pwm_value) + ":" + str(pwm_value)
                SET_REAGENT_ZONE_PID_CMD = "TRPIDPIDCL22500:3000:4000:50:" + str(pwm_value) + ":" + str(pwm_value)
            elif pwm_value == 100:
                SET_SENSOR_ZONE_PID_CMD = "TSPIDPIDCL24500:3000:4000:50:100:100"
                SET_REAGENT_ZONE_PID_CMD = "TRPIDPIDCL24500:3000:4000:50:100:100"

            success = self.send_tm_serial(SET_SENSOR_ZONE_PID_CMD)
            success = self.send_tm_serial(SET_REAGENT_ZONE_PID_CMD)
            return success

        except Exception as e:
            raise Exception("Error set_cool_tec_pid(): " + str(e))

    def set_heat_tec_pid(self, pwm_value):
        """
        For now:
        - Both Sensor and Reagent zones together
        - Only setting PWM, fixed P,I,D values to 500:3000:4000
        """
        try:
            if pwm_value < 100:
                SET_SENSOR_ZONE_PID_CMD = "TSPIDPIDHT22500:3000:4000:50:" + str(pwm_value) + ":" + str(pwm_value)
                SET_REAGENT_ZONE_PID_CMD = "TRPIDPIDHT22500:3000:4000:50:" + str(pwm_value) + ":" + str(pwm_value)
            elif pwm_value == 100:
                SET_SENSOR_ZONE_PID_CMD = "TSPIDPIDHT24500:3000:4000:50:100:100"
                SET_REAGENT_ZONE_PID_CMD = "TRPIDPIDHT24500:3000:4000:50:100:100"

            success = self.send_tm_serial(SET_SENSOR_ZONE_PID_CMD)
            success = self.send_tm_serial(SET_REAGENT_ZONE_PID_CMD)
            return success

        except Exception as e:
            raise Exception("Error set_heat_tec_pid(): " + str(e))

    # For other modules to use SPI channel
    def get_peripheral_ftdi_spi_channel(self):
        port_url = self.connected_devices[PERIPHERAL_FTDI_URL_INEX][SPI_CHANNEL_INDEX]
        return port_url

    # For other modules to use I2C channel
    def get_peripheral_ftdi_i2c_channel(self):
        port_url = self.connected_devices[PERIPHERAL_FTDI_URL_INEX][I2C_CHANNEL_INDEX]
        return port_url

    #  Turns the fans down and thermal off
    def quiet_mode(self):
        try:
            # Turn off the thermal module
            resp = self.send_tm_serial(msg = "TSENABLE__10")
            resp = self.send_tm_serial(msg = "TRENABLE__10")

            # Turn fans to low
            resp = self.send_tm_serial(msg = "FN1PWM____210")
            resp = self.send_tm_serial(msg = "FN3PWM____210")
            resp = self.send_tm_serial(msg = "FN4PWM____210")

            # Turn lightbar pink
            self.turn_lightbar_all_on(r = 153, g = 0, b = 204) 
        except Exception as e:
            raise Exception("Error quiet_mode(): " + str(e))


        