#!/usr/bin/python3
class PrintColours():
    COLOUR_DEFAULT = "\033[39m"
    COLOUR_FAIL = "\033[31m"
    COLOUR_PASS = "\033[32m"
    COLOUR_INFO = "\033[34m"   

def print_info(text):
    print(PrintColours.COLOUR_INFO + text + PrintColours.COLOUR_DEFAULT)

def print_fail(text):
    print(PrintColours.COLOUR_FAIL + text + PrintColours.COLOUR_DEFAULT)

def print_pass(text):
    print(PrintColours.COLOUR_PASS + text + PrintColours.COLOUR_DEFAULT)