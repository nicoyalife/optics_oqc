import inquirer

# Test Input options
FULL_SUITE = "full_suite"
LOOP_TEST_CARTRIDGE_WORKFLOW = "loop_test_cartridge_workflow"
LOOP_FLUIDIC_CARTRIDGE_WORKFLOW = "loop_fluidic_cartridge_workflow"
CARTRIDGE_ENGAGE = "cartridge_engage"
CARTRIDGE_DISENGAGE = "cartridge_disengage"
LOOP_CARTRIDGE_ENGAGE_DISENGAGE = "cartridge_engage_disengage_loop"
TEST_CARTRIDGE_SCAN = "test_cartridge_scan"
LIGHT_BAR_ON = "lightbar_on"
LIGHT_BAR_OFF = "lightbar_off"
ADJUST_TEC_PWM_30 = "adjust tec pwm to 30%"
ADJUST_TEC_PWM_75 = "adjust tec pwm to 75%"
ADJUST_TEC_PWM_100 = "adjust tec pwm to 100%"
RUN_EW_RECIPE = "run_ew_recipe"
QUIET_MODE = "quiet_mode"
EXIT = "exit"

# Function to create interactive command line user interface 
def interactive_command_line():
    try:
        # prompt list to show
        questions = [
            inquirer.List(
                'tests',
                message = 'Select Test Method',
                choices = [FULL_SUITE,
                            LOOP_TEST_CARTRIDGE_WORKFLOW,
                            LOOP_FLUIDIC_CARTRIDGE_WORKFLOW,
                            CARTRIDGE_ENGAGE,
                            CARTRIDGE_DISENGAGE,
                            LOOP_CARTRIDGE_ENGAGE_DISENGAGE,
                            TEST_CARTRIDGE_SCAN, 
                            LIGHT_BAR_ON,
                            LIGHT_BAR_OFF,
                            ADJUST_TEC_PWM_30, 
                            ADJUST_TEC_PWM_75,
                            ADJUST_TEC_PWM_100,
                            RUN_EW_RECIPE,
                            QUIET_MODE,
                            EXIT]
                ),
            ]
        
        #Show command interface
        answers = inquirer.prompt(questions)

        # Return user input 
        return answers.get('tests')
    except: 
        print("Error in interactive_command_line()")
