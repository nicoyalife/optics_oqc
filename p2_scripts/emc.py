#!/usr/bin/python3

import sys
from common.cmd_line_ui import interactive_command_line
from common.alto_core import AltoCore
from common.print_colours import *

#from engage_disengage_cartridge import load_cartridge, eject_cartridge
from engage_disengage_cartridge import *
from test_cartridge_test import *
from run_ew_recipe import *
from util.pygetchar import _Getch


# Test Input options
FULL_SUITE = "full_suite"
LOOP_TEST_CARTRIDGE_WORKFLOW = "loop_test_cartridge_workflow"
LOOP_FLUIDIC_CARTRIDGE_WORKFLOW = "loop_fluidic_cartridge_workflow"
CARTRIDGE_ENGAGE = "cartridge_engage"
CARTRIDGE_DISENGAGE = "cartridge_disengage"
LOOP_CARTRIDGE_ENGAGE_DISENGAGE = "cartridge_engage_disengage_loop"
TEST_CARTRIDGE_SCAN = "test_cartridge_scan"
LIGHT_BAR_ON = "lightbar_on"
LIGHT_BAR_OFF = "lightbar_off"
ADJUST_TEC_PWM_30 = "adjust tec pwm to 30%"
ADJUST_TEC_PWM_75 = "adjust tec pwm to 75%"
ADJUST_TEC_PWM_100 = "adjust tec pwm to 100%"
RUN_EW_RECIPE = "run_ew_recipe"
QUIET_MODE = "quiet_mode"
EXIT = "exit"

DEFAUT_ASSUMPTIONS = " Default assumptions : \n \
        - Light bar is always on 100% WHITE \n \
        - Front and back IR LED illuminations are always on at 1V8 \n \
        - Thermal module min max are set to 50% \n \
        - TEC Fan is set to 90%  \n \
        - TEC temperature target is set to 4C on initiation \n \
        - Intake Fans are set to 95% \n \
        - HSI illumination DAC is set to 1V8"

# Fluidic recipe
EW_TEST_RECIPE = "/config/test_nico_file.nico"

def wait_for_spacebar_input():
    # loop until user input space bar
    getch = _Getch()
    while True:
        print_info("Enter spacebar to continue")
        ch = getch()
        if(ch == " "):
            return True
        else:
            print_info("Wrong input, please input SPACEBAR to continue")
            
def menu_input():
    # Get working directory 
    dir_path = os.path.dirname(os.path.realpath(__file__))

    # Test selection input from the user 
    user_input = []
    user_input = interactive_command_line()
 
    if user_input == EXIT:
        print_info("Stopped the EMC test.")
        sys.exit(0)
    if user_input == FULL_SUITE:
        # Run full suite
        full_suite(alto_core = alto_core)
    elif user_input == LOOP_TEST_CARTRIDGE_WORKFLOW:
        # Loop test cartridge workflow
        loop_test_cartridge_workflow(alto_core = alto_core)
    elif user_input == LOOP_FLUIDIC_CARTRIDGE_WORKFLOW:
        # Loop the fluidic cartridge workflow
        loop_fluidic_cartridge_workflow(alto_core = alto_core)
    elif user_input == CARTRIDGE_ENGAGE:
        # load cartridge
        load_cartridge(alto_core = alto_core)
    elif user_input == CARTRIDGE_DISENGAGE:
        # unload cartridge
        eject_cartridge(alto_core = alto_core)
    elif user_input == LOOP_CARTRIDGE_ENGAGE_DISENGAGE:
        # run cartridge load nad unload experiment in a loop until user pressing Ctl-C
        try:
            while True:
                # disengage the cartridge
                success = eject_cartridge(alto_core = alto_core)
                if success != True:
                    raise Exception("Unable to eject cartridge shuttle.")

                # Request user to load a test cartridge
                print_info("Load in a test cartridge and click spacebar once the cartridge has been loaded.")
                wait_for_spacebar_input()

                # engage the test cartridge
                success = load_cartridge(alto_core = alto_core)
                if success != True:
                    raise Exception("Unable to load the test cartridge.")
        except KeyboardInterrupt:
            print_info("Stopped the loop of fluidic experiment.")
            
    elif user_input == TEST_CARTRIDGE_SCAN:
        test_cartrige_full_scan(alto_core = alto_core)
    elif user_input == LIGHT_BAR_ON:
        toggle_light_bar("on")
    elif user_input == LIGHT_BAR_OFF:
        toggle_light_bar("off")
    elif user_input == ADJUST_TEC_PWM_30:
        adjust_tec_pwm(alto_core = alto_core, pwm_set_point = 30)
    elif user_input == ADJUST_TEC_PWM_75:
        adjust_tec_pwm(alto_core = alto_core, pwm_set_point = 75)
    elif user_input == ADJUST_TEC_PWM_100:
        adjust_tec_pwm(alto_core = alto_core, pwm_set_point = 100)
    elif user_input == RUN_EW_RECIPE:
        run_ew_recipe_routine(alto_core = alto_core, recipe_to_run = dir_path + EW_TEST_RECIPE)
    elif user_input == QUIET_MODE:
        alto_core.quiet_mode()

def run_ew_recipe_routine(alto_core, recipe_to_run, display_header = True):
    try:
        run_ew_recipe(alto_core = alto_core, recipe_file = recipe_to_run)
    except Exception as e:
        raise Exception(e)

def adjust_tec_pwm(alto_core, pwm_set_point):
    """ adjust pid parameters, only pwm for now. """
    try:
        resp = alto_core.set_cool_tec_pid(pwm_value = pwm_set_point)
        #alto_core.set_heat_tec_pid(pwm_value = pwm_set_point)
        resp_list = resp.split(":")
        if int(resp_list[-1]) == int(resp_list[-2]) and int(resp_list[-1]) == pwm_set_point:
            # Write success message
            print_pass("===========================================================")
            print_pass("Set TEC Zones to %d PASSED" % pwm_set_point)
            print_pass("===========================================================")
        else:
            raise Exception("Unable to load the fluidic cartridge.")

    except Exception as e:
        raise Exception(e)

def full_suite(alto_core):
    try:       
        # Get working directory 
        dir_path = os.path.dirname(os.path.realpath(__file__))

        # clear terminal
        os.system('cls' if os.name == 'nt' else 'clear')

        # Print test header
        print_info("===========================================================")
        print_info("EMC Full Suite")        
        print_info("===========================================================")

        # disengage the cartridge
        success = eject_cartridge(alto_core = alto_core)
        if success != True:
            raise Exception("Unable to eject cartridge shuttle.")

        # Request user to load a test cartridge
        print_info("Load in a test cartridge and click spacebar once the cartridge has been loaded.")
        wait_for_spacebar_input()
        
        # engage the test cartridge
        success = load_cartridge(alto_core = alto_core)
        if success != True:
            raise Exception("Unable to load the test cartridge.")

        # Run the full test cartridge scan
        success = test_cartrige_full_scan(alto_core = alto_core)

        # Disengage the test cartridge
        success = eject_cartridge(alto_core = alto_core)
        if success != True:
            raise Exception("Unable to eject the test cartridge.")

        # Request user to load a test cartridge
        print_info("Load in a fluidic cartridge and click spacebar once the cartridge has been loaded.")
        wait_for_spacebar_input()

        # engage the fluidic cartridge
        success = load_cartridge(alto_core = alto_core)
        if success != True:
            raise Exception("Unable to load the fluidic cartridge.")

        # run fluidic experiment in a loop until user pressing Ctl-C
        try:
            while True:
                print_info("Press Ctl-C to stop loop")
                run_ew_recipe_routine(alto_core = alto_core, recipe_to_run = dir_path + EW_TEST_RECIPE)
                print_info("Start over fluidic experiment again")
        except KeyboardInterrupt:
            print_info("Stopped the loop of fluidic experiment.")
            
        # Disengage the test cartridge
        success = eject_cartridge(alto_core = alto_core)
        if success != True:
            raise Exception("Unable to eject the test cartridge.")

        if success == True:
            # Write success message
            print_pass("===========================================================")
            print_pass("EMC Full Suite PASSED")
            print_pass("===========================================================")

    except Exception as e:
        print_fail("===========================================================")
        print_fail("EMC Full Suite FAILED")
        print_fail("Error = " + str(e))
        print_fail("===========================================================")
        
def loop_test_cartridge_workflow(alto_core):
    header_text = "Test Cartridge Workflow"
    try:
        # clear terminal
        os.system('cls' if os.name == 'nt' else 'clear')

        # Print test header
        print_info("===========================================================")
        print_info(header_text)        
        print_info("===========================================================")

        # disengage the cartridge
        success = eject_cartridge(alto_core = alto_core)
        if success != True:
            raise Exception("Unable to eject cartridge shuttle.")

        # Request user to load a test cartridge
        print_info("Load in a test cartridge and click spacebar once the cartridge has been loaded.")
        wait_for_spacebar_input()

        # Run the infinite loop
        print("The program will have to be killed with CNTRL+Z.")
        input("Press ENTER to start the loop.")
        while True:          
            # Load the cartridge
            success = load_cartridge(alto_core = alto_core)
            if success != True:
                raise Exception("Unable to load the test cartridge.")     
           
            # Run the test cartridge scan
            test_cartrige_full_scan(alto_core = alto_core)

            # Eject the cartridge
            success = eject_cartridge(alto_core = alto_core)
            if success != True:
                raise Exception("Unable to eject the test cartridge.")    
    except expression as identifier:
        print_fail("===========================================================")
        print_fail(header_text + " FAILED")
        print_fail("Error = " + str(e))
        print_fail("===========================================================")

def loop_fluidic_cartridge_workflow(alto_core):
    # Get working directory 
    dir_path = os.path.dirname(os.path.realpath(__file__))
    
    # Header text
    header_text = "Fluidic Cartridge Workflow"
    try:
        # clear terminal
        os.system('cls' if os.name == 'nt' else 'clear')

        # Print test header
        print_info("===========================================================")
        print_info(header_text)        
        print_info("===========================================================")

        # disengage the cartridge
        success = eject_cartridge(alto_core = alto_core)
        if success != True:
            raise Exception("Unable to eject cartridge shuttle.")

        # Request user to load a test cartridge
        print_info("Load in a fluidic cartridge and click spacebar once the cartridge has been loaded.")
        wait_for_spacebar_input()

        # Run the infinite loop
        print("The program will have to be killed with CNTRL+Z.")
        input("Press ENTER to start the loop.")
        while True:          
            # Load the cartridge
            success = load_cartridge(alto_core = alto_core)
            if success != True:
                raise Exception("Unable to load the fluidic cartridge.")     
           
            run_ew_recipe_routine(alto_core = alto_core, recipe_to_run = dir_path + EW_TEST_RECIPE, display_header = False)

            # Eject the cartridge 
            success = eject_cartridge(alto_core = alto_core)
            if success != True:
                raise Exception("Unable to eject the fluidic cartridge.")                

    except expression as identifier:
        print_fail("===========================================================")
        print_fail(header_text + " FAILED")
        print_fail("Error = " + str(e))
        print_fail("===========================================================")

def toggle_light_bar(op):
    """
    accepted operations:
        - on = 100% blue
        - off = all off
    """
    alto_core.toggle_light_bar(op)


if __name__ == '__main__':
    try:
        # Get working directory 
        dir_path = os.path.dirname(os.path.realpath(__file__))

        # Success flag
        success = True

        # clear terminal
        os.system('cls' if os.name == 'nt' else 'clear')

        # Print connecting message
        print_info("Connecting please wait...")
        
        # Create a new alto core object
        alto_core = AltoCore()

        # Disengage the cartridge off start of emc
        eject_cartridge(alto_core = alto_core)

        # clear terminal
        os.system('cls' if os.name == 'nt' else 'clear')

        # Print test header
        print_info("===========================================================")
        print_info("EMC Test")  
        print_info(DEFAUT_ASSUMPTIONS)          
        print_info("===========================================================")

        # Loop user interface until input is "Exit"
        try:
            while True:
                # clear terminal
                os.system('cls' if os.name == 'nt' else 'clear')

                # Print the menu
                menu_input()
        except KeyboardInterrupt:
            print_info("Stopped the EMC test.")

    except Exception as identifier:
        print(identifier)