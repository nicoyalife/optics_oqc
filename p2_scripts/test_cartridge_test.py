#!/usr/bin/python3
import serial
import os
import sys
import time

from common.alto_core import AltoCore

# test constants
NUM_HVOUTS = 256
EMPTY_MUX_VAL = '0000000000000000000000000000000000000000000000000000000000000000'
WHO_CMD = "HVWHO?____0"
WHO_RESP = "HV"
MUX_CMD = "HVMUX_____X"
STABILIZATION_TIME = 0.004
# +HV SENSE
TC_SINGL_CMD = "TC+HVSINGL1"
EXPECTED_POSITIVE_VOLTAGE = 300
OFFSET_POSITIVE_HV_PERCENTAGE = 0.03
# Test cartridge voltage rail sensing
TC_VOLTS_CMD = "TCVOLTS____0"
VOLTAGE_RAIL_DELIMITERS = ' '
EXPECTED_RAIL_RESULT_COUNT = 3
RESP_2V5_INDEX = 0
RESP_3V3_INDEX = 1
RESP_5V_INDEX = 2
EXPCETED_2V5_RAIL = 2.5
EXPECTED_3V3_RAIL = 3.3
EXPECTED_5V_RAIL = 5.0
OFFSET_RAIL_PERCENTAGE = 0.02
# Test cartridge temperature sensing
TC_TEMP_SENSE_CMD = "TCTEMPS___0"
TEMP_DELIMITERS = ' '
EXEPECTED_TEMPS = 8
TC_NEG_SENSE_CMD = "TC-HVSENSE0"
TC_NEG_MUX_VAL = "HVMUX_____X0000000000000000000000000000000000000000000000000000000000000001"


# variables
ser = 0

class PrintColours():
    COLOUR_DEFAULT = "\033[39m"
    COLOUR_FAIL = "\033[31m"
    COLOUR_PASS = "\033[32m"
    COLOUR_INFO = "\033[34m"   

def print_info(text):
    print(PrintColours.COLOUR_INFO + text + PrintColours.COLOUR_DEFAULT)

def print_fail(text):
    print(PrintColours.COLOUR_FAIL + text + PrintColours.COLOUR_DEFAULT)

def print_pass(text):
    print(PrintColours.COLOUR_PASS + text + PrintColours.COLOUR_DEFAULT)

def __positive_hv_sense__(alto_core, hv_out):
    # Get the mux string for that output
    # get the binary value of that hvout
    bin_val = 0b1 << hv_out
    # Get the hex value
    hex_val = hex(bin_val)
    # convert to string
    mux_string = str(hex_val)
    # Drop the 0x part of the mux string
    mux_string = mux_string.replace("0x", "")

    # add leading 0s
    for _ in range(0, len(EMPTY_MUX_VAL)):
        full_len = len(EMPTY_MUX_VAL)
        current_len = len(mux_string)
        if current_len < full_len:
            mux_string = '0' + mux_string

    # sanity check the length of msg
    if len(mux_string) != (NUM_HVOUTS / 8) * 2:
        raise Exception("Invalid expected mux string size.")

    # Add the header to the cmd
    cmd = MUX_CMD + mux_string
    # Send the 
    resp = send_serial_msg(alto_core = alto_core, msg = cmd)

    # Wait for the voltage to stabilize
    time.sleep(STABILIZATION_TIME)

    # Sense cmd 
    sense_cmd = TC_SINGL_CMD + str(hv_out)
    # Send the sense cmd
    resp = send_serial_msg(alto_core = alto_core, msg = sense_cmd)

    # Error reading +HV
    if 'ERR' in resp:
        raise Exception(resp)

    # Validate it is within the error
    if float(resp) < EXPECTED_POSITIVE_VOLTAGE - (EXPECTED_POSITIVE_VOLTAGE * OFFSET_POSITIVE_HV_PERCENTAGE) or \
        float(resp) > EXPECTED_POSITIVE_VOLTAGE + (EXPECTED_POSITIVE_VOLTAGE * OFFSET_POSITIVE_HV_PERCENTAGE):
        raise Exception("Measured a failure on HVOUT" + str(hv_out) + ". Measured value = " + str(resp))

    # Print info of that HVOUT
    print_info("HVOUT" + str(hv_out) + ". Measured value = " + str(resp))

# Method will test all positive senses using single method
def test_positive_sense_singles(alto_core):
    success = True
    for hv_out in range(0, NUM_HVOUTS):
        try:
            __positive_hv_sense__(alto_core = alto_core, hv_out = hv_out)
            time.sleep(STABILIZATION_TIME)
        except Exception as e:
            success = False
            print_fail("Error HVOUT" + str(hv_out) + ": " + str(e))
    # Return the success 
    return success

# Method to verify the test cartridge rail voltages
def test_cartridge_voltage_rails(alto_core):
    success = True

    # Send the voltage rail request
    resp = send_serial_msg(alto_core = alto_core, msg = TC_VOLTS_CMD)

    # splIt the voltage response
    split_resp = resp.split(VOLTAGE_RAIL_DELIMITERS)

    # split the response values
    if len(split_resp) != EXPECTED_RAIL_RESULT_COUNT:
        print_fail("Error invalid voltage rail size.")
        success = False
    rail_2v5 = split_resp[RESP_2V5_INDEX]
    rail_3v3 = split_resp[RESP_3V3_INDEX]
    rail_5v = split_resp[RESP_5V_INDEX]

    # Compare the response values against the acceptable percentage
    if float(rail_2v5) < EXPCETED_2V5_RAIL - (EXPCETED_2V5_RAIL * OFFSET_RAIL_PERCENTAGE) or \
        float(rail_2v5) > EXPCETED_2V5_RAIL + (EXPCETED_2V5_RAIL * OFFSET_RAIL_PERCENTAGE):
        success = False
    if float(rail_3v3) < EXPECTED_3V3_RAIL - (EXPECTED_3V3_RAIL * OFFSET_RAIL_PERCENTAGE) or \
        float(rail_3v3) > EXPECTED_3V3_RAIL + (EXPECTED_3V3_RAIL * OFFSET_RAIL_PERCENTAGE):
        success = False
    if float(rail_5v) < EXPECTED_5V_RAIL - (EXPECTED_5V_RAIL * OFFSET_RAIL_PERCENTAGE) or \
        float(rail_5v) > EXPECTED_5V_RAIL + (EXPECTED_5V_RAIL * OFFSET_RAIL_PERCENTAGE):
        success = False
    
    # Print info of the voltage rail readings
    print_info("Voltage rail measued 2V5 = " + str(rail_2v5) + "V")
    print_info("Voltage rail measued 3V3 = " + str(rail_3v3) + "V")
    print_info("Voltage rail measued 5V = " + str(rail_5v) + "V")
    
    # Return the success 
    return success

# Test temperature readings
def test_cartirdge_temperatures(alto_core):
    success = True

    # Send the request of the test cartridge temperatures
    resp = send_serial_msg(alto_core = alto_core, msg = TC_TEMP_SENSE_CMD)

    # Split the response
    split = resp.split(TEMP_DELIMITERS)

    # Check for the length matching expected
    if len(split) != EXEPECTED_TEMPS:
        print_fail("Error invalid temperature sense result size.")
        success = False

    print_info("Test cartridge temperature sensors = " + str(resp))

    # Return the success
    return success

# Method will negative sense test cartridge voltage
def test_cartridge_negative_hv_sense(alto_core):
    success = True

    # Mux the output
    resp = send_serial_msg(alto_core = alto_core, msg = TC_NEG_MUX_VAL)

    time.sleep(STABILIZATION_TIME)

    # Negative sense
    resp = send_serial_msg(alto_core = alto_core, msg = TC_NEG_SENSE_CMD)

    # log the sense result
    print_info("Test cartridge negative sense = " + str(resp))

    # Return the success
    return success

def send_serial_msg(alto_core, msg):
    try:
        resp = alto_core.send_hv_serial(msg)
        return resp
    except Exception as e:
        raise Exception("Error send_serial_msg(): " + str(e))

def test_cartrige_full_scan(alto_core):
    success = True
    try:
        # Test the test cartridge rail feedback
        if test_cartridge_voltage_rails(alto_core = alto_core) != True:
            success = False

        # Test the test cartridge temperature sensors
        if test_cartirdge_temperatures(alto_core = alto_core) != True:
            success = False

        # Test positive hv sense single methods
        if test_positive_sense_singles(alto_core = alto_core) != True:
            success = False

        # Negative hv sense
        if test_cartridge_negative_hv_sense(alto_core = alto_core) != True:
            success = False
    except Exception as e:
        raise Exception(e)

    # Return the success flag
    return success

if __name__ == '__main__':
    try:
        success = True
        # clear terminal
        os.system('cls' if os.name == 'nt' else 'clear')

        # Print test header
        print_info("===========================================================")
        print_info("Test Cartridge Test")
        print_info("===========================================================")

        # Create a new alto core object
        alto_core = AltoCore()

        # Test for hello
        resp = send_serial_msg(alto_core = alto_core, msg = WHO_CMD)
        if not resp == WHO_RESP:
            raise Exception("Invalid who response was received.")
        
        # Run the full test cartridge scan
        success = test_cartrige_full_scan(alto_core = alto_core)
        
        if success == True:                
            # Write success message
            print_pass("===========================================================")
            print_pass("Test Cartridge Test PASSED")
            print_pass("===========================================================")

    except Exception as e:
        print_fail("===========================================================")
        print_fail("Test Cartridge Test FAILED")
        print_fail("Error = " + str(e))
        print_fail("===========================================================")