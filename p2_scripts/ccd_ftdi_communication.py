#!/usr/bin/python3
import os
import time
import sys
import subprocess
import signal

from common.alto_core import AltoCore
from common.print_colours import*

loop = False

# Constants
TESTING_MESSAGE = "T"

# Test ccb board communication by shorting TX and RX pin on board 
def ccb_ftdi_com_testing(alto_core):
    print_info("############ CCB ftdi communication testing start ############")

    # Success flag
    success = True

    #Run loop to test ccb board ftdi communication 
    while True == True:
        try:
            print("[Sending]: " + TESTING_MESSAGE)
            #Send serial data
            resp = alto_core.send_hv_serial(TESTING_MESSAGE)
            print ("[Response]: " + resp)

            #Delay
            time.sleep(2)

            #Compare response 
            if resp != TESTING_MESSAGE:
                return False

        except Exception as e:
            raise "Error ccb_ftdi_com_testing(): " + str(e)

    print_info("############ CCB ftdi communication testing end ############")
    # return the success flag
    return success

if __name__ == '__main__':
    # Get the header text
    header_text = "ccb_com_test"
   
    if loop == True:
        header_text = "loop"
        
    try:
        success = True
        # clear terminal
        os.system('cls' if os.name == 'nt' else 'clear')

        # Print test header
        print_info("===========================================================")
        print_info(header_text)
        print_info("===========================================================")
    
        # Create a new alto core object
        alto_core = AltoCore()

        if loop == True:
            count = 0
            while True == True:
                #Send serial data
                print("Sending serial data")
        if success == True:                
            # Write success message
            print_pass("===========================================================")
            print_pass(header_text + " PASSED")
            print_pass("===========================================================")
    except Exception as e:
        print_fail("===========================================================")
        print_fail(header_text + " FAILED")
        print_fail("Error = " + str(e))
        print_fail("===========================================================")     