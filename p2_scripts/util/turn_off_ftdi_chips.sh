# USB Hub Enable
cd /sys/class/gpio/gpio510/
echo out > direction
sleep 1
echo 0 > value
# USB Hub reset
cd /sys/class/gpio/gpio511/
echo out > direction
sleep 1
echo 0 > value

# FTDI A Enable
cd /sys/class/gpio/gpio504/
echo out > direction
sleep 1
echo 0 > value
# FTDI A Reset
cd /sys/class/gpio/gpio505/
echo out > direction
sleep 1
echo 0 > value

# FTDI B Enable
cd /sys/class/gpio/gpio506/
echo out > direction
sleep 1
echo 0 > value
# FTDI B Reset
cd /sys/class/gpio/gpio507/
echo out > direction
sleep 1
echo 0 > value