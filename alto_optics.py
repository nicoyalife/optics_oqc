#!/usr/bin/python3
from pypylon import pylon
from pypylon import genicam
#from airtable import Airtable
import numpy as np
import pandas as pd
import time
from pathlib import Path
import glob
import pickle
import string

import sys
from p2_scripts.common.cmd_line_ui import interactive_command_line
from p2_scripts.common.alto_core import AltoCore
from p2_scripts.common.print_colours import *

#from engage_disengage_cartridge import load_cartridge, eject_cartridge
from engage_disengage_cartridge import *

class hsi:
    # Initilizes the camera and sets the correct pixel format anda  default exposure time
    def __init__(self):
        self.camera = pylon.InstantCamera(pylon.TlFactory.GetInstance().CreateFirstDevice())
        self.camera.Open()
        self.camera.SequencerMode = "Off"
        self.camera.SequencerConfigurationMode = "Off"
        self.camera.PixelFormat.SetValue("Mono12")
        self.camera.ExposureTime.SetValue(10000.0)

    # Sets exposure time on hsi
    def SetExposureTime(self, timeinuseconds):
        self.camera.ExposureTime.SetValue(timeinuseconds)
        
    # Sets exposure time on hsi        
    def SetGain(self,gain):
        self.camera.GainRaw.SetValue(gain)
        
    # Grabs image from camera. Returns numpy matrix
    def GetImage(self):
        timeout=100
        img=self.camera.GrabOne(timeout)
        return img.Array
    
    # Returns serial number of camera
    def GetSerialNumber(self):
        x=self.camera.GetDeviceInfo().GetSerialNumber()
        return x
    
    # Returns Spectrometer Serial Number
    def GetSpecSN(self):
        self.camera.UserDefinedValueSelector.SetValue("Value1")
        x=self.camera.UserDefinedValue.GetValue()
        return x
    
    # Returns Coefficient 0
    def GetC0(self):
        self.camera.UserDefinedValueSelector.SetValue("Value2")
        x=self.camera.UserDefinedValue.GetValue()
        y=x/1E4
        return y
    
    # Returns Coefficient 1
    def GetC1(self):
        self.camera.UserDefinedValueSelector.SetValue("Value3")
        x=self.camera.UserDefinedValue.GetValue()
        y=x/1E6
        return y
        
    # Returns Coefficient 2
    def GetC2(self):
        self.camera.UserDefinedValueSelector.SetValue("Value4")
        x=self.camera.UserDefinedValue.GetValue()
        y=x/(-1E10)
        return y
    
    def GetOptImage(self,low_guess,high_guess):
        self.camera.PixelFormat.SetValue("Mono12")
        if genicam.IsWritable(self.camera.OffsetX):
                self.camera.OffsetX = self.camera.OffsetX.Min
        self.camera.Width = self.camera.Width.Max
        
        if genicam.IsWritable(self.camera.OffsetY):
                self.camera.OffsetY = self.camera.OffsetY.Min
        self.camera.Height = self.camera.Height.Max
        
        timeout=100
        
        self.camera.ExposureTime.SetValue(low_guess)
        low_image=self.camera.GrabOne(timeout).Array
        
        self.camera.ExposureTime.SetValue(high_guess)
        high_image=self.camera.GrabOne(timeout).Array
        
        max_count_low=np.amax(low_image)
        max_count_high=np.amax(high_image)
        target_max=3500
        estimated_optimal_exposure_time=target_max*(high_guess-low_guess)/(max_count_high-max_count_low)
                
        self.camera.ExposureTime.SetValue(estimated_optimal_exposure_time)
        opt_img=self.camera.GrabOne(timeout).Array
        
        return (opt_img,np.int(estimated_optimal_exposure_time))

    def GetImageSeq(self,ChannelMap,ExposureTimes=[]):
        
        if ExposureTimes==[]:
            LoadExpTimes=False
        else:
            LoadExpTimes=True
            
        self.camera.Open()
        self.camera.SequencerMode = "Off"
        self.camera.SequencerConfigurationMode = "Off"
        self.camera.PixelFormat.SetValue("Mono12")
        self.camera.ExposureTime.SetValue(100)

        # enable camera chunk mode
        self.camera.ChunkModeActive = True
        # enable exposuretime chunk
        self.camera.ChunkSelector = "ExposureTime"
        self.camera.ChunkEnable = True

        self.camera.UserSetSelector = "Default"
        self.camera.UserSetLoad.Execute()
        self.camera.PixelFormat.SetValue("Mono12")
        self.camera.SequencerMode = "Off"
        self.camera.SequencerConfigurationMode = "On"
        
        
       

        df=ChannelMap[1]
        row = next(df.iterrows())[1]
        for count,channel in enumerate(df.iterrows()):                
            self.camera.SequencerSetSelector = count
            if not LoadExpTimes:
                self.camera.ExposureTime=(channel[1]['OptimalExposureTimes'])
            else:
                self.camera.ExposureTime=ExposureTimes[count]
            
            if genicam.IsWritable(self.camera.OffsetX):
                self.camera.OffsetX = self.camera.OffsetX.Min
            self.camera.Width = self.camera.Width.Max
            
            
            Height=np.int((np.floor(channel[1]['ColumnCount'])/2)*2)
            self.camera.Height = Height
            if genicam.IsWritable(self.camera.OffsetY):
                OffsetY=np.int((np.floor((channel[1]['ColumnStart'])/2))*2)
                self.camera.OffsetY = OffsetY
            
            if count==(len(df.index)-1):
                self.camera.SequencerPathSelector = 1
                self.camera.SequencerSetNext = 0
            else:
                pass
            self.camera.SequencerSetSave.Execute()
            
            
        self.camera.SequencerConfigurationMode = "Off"
        self.camera.SequencerMode = "On"

        self.camera.StartGrabbingMax(15)
        ImageArray=[]
        while self.camera.IsGrabbing():
            res = self.camera.RetrieveResult(1000)
            ImageArray.append(res.Array)
            res.Release()
            
        self.camera.StopGrabbing()
        self.camera.SequencerMode = "Off"
        self.camera.SequencerConfigurationMode = "Off"
        self.camera.Close()
        return(ImageArray)
        
    
    # Closes camera communication 
    def Close(self):
        self.camera.Close()
        
class AirtableDatabase:
    def __init__(self):
        api_key='keyrHDYbrQgyw1XCW'
        base_key='appuFJHov2xh4vNtT'
        acceptance_criteria_table_name='Acceptance Criteria'
        performance_table_name='Optical Setup Performance'
        
        self.acceptance_criteria_db=Airtable(base_key,acceptance_criteria_table_name,api_key)
        self.performance_db=Airtable(base_key,performance_table_name,api_key)
        
    def GetExposureTimeCritera(self):
        x=self.acceptance_criteria_db.search('Name','exposure_time')[0].get('fields').get('Value')
        return x

    def GetChannelVarianceCritera(self):
        x=self.acceptance_criteria_db.search('Name','channel_variance')[0].get('fields').get('Value')
        return x
    
class ChannelMap:
    def __init__(self):
        
        self.CutoffPercentage=80
        self.SpectralIndex=350
        self.ChannelThreshold=20
        self.TargetCounts= 3500
        self.ChannelMapFolder=Path('./ChannelMaps')
        self.ExpTimeArray=[]
        
    def GenerateCM(self, Image,ExposureTime):
    
        opt_image=Image
        
        SpatialProfile=opt_image[:,self.SpectralIndex]


        a=np.array(SpatialProfile)
        a[a <= self.ChannelThreshold] = 0
        a[a > self.ChannelThreshold] = 1

        ChannelStarts=[]
        ChannelEnds=[]
        for counter,value in enumerate(a):
          if a[(counter)] > a[(counter-1)]:
            ChannelStarts.append(counter)
          elif a[(counter)] < a[(counter-1)]:
            ChannelEnds.append(counter)

        RowStart=np.zeros(len(ChannelStarts))
        RowCount=np.ones(len(ChannelStarts))*719
        ColumnCount=np.subtract(ChannelEnds,ChannelStarts)
        ColumnStart=ChannelStarts
        
        ChannelMaxs=[]
        for count,chstart in enumerate(ChannelStarts):
            ChannelProfile=SpatialProfile[ChannelStarts[count]:ChannelEnds[count]]
            ChannelMax=np.amax(ChannelProfile)
            ChannelMaxs.append(ChannelMax)
            
        MaxCountsPerMicrosecond=np.divide(ExposureTime,ChannelMaxs)
        OptimalExposureTimes=np.multiply(MaxCountsPerMicrosecond,self.TargetCounts)
        
        import pandas as pd
        d = {'RowStart': RowStart,'ColumnStart':ColumnStart, 'RowCount': RowCount, 'ColumnCount':ColumnCount,'OptimalExposureTimes':OptimalExposureTimes}
        df = pd.DataFrame(data=d)
              
        

        self.ChannelMapFolder.mkdir(parents=True, exist_ok=True)
        ChannelMapFileName="ChannelMap_"+time.strftime("%Y_%m_%d__%H_%M_%S", time.localtime())+".csv"
        File=Path.joinpath(self.ChannelMapFolder,ChannelMapFileName)
        
        self.ExpTimeArray=OptimalExposureTimes
        
        df.to_csv(File)
        
        return(df)
    
    def GenerateOptimalExp(self, Image,ExposureTime):
    
        opt_image=Image
        SpatialProfile=opt_image[:,self.SpectralIndex]


        a=np.array(SpatialProfile)
        a[a <= self.ChannelThreshold] = 0
        a[a > self.ChannelThreshold] = 1

        ChannelStarts=[]
        ChannelEnds=[]
        for counter,value in enumerate(a):
          if a[(counter)] > a[(counter-1)]:
            ChannelStarts.append(counter)
          elif a[(counter)] < a[(counter-1)]:
            ChannelEnds.append(counter)

        RowStart=np.zeros(len(ChannelStarts))
        RowCount=np.ones(len(ChannelStarts))*719
        ColumnCount=np.subtract(ChannelEnds,ChannelStarts)
        ColumnStart=ChannelStarts
        
        ChannelMaxs=[]
        for count,chstart in enumerate(ChannelStarts):
            ChannelProfile=SpatialProfile[ChannelStarts[count]:ChannelEnds[count]]
            ChannelMax=np.amax(ChannelProfile)
            ChannelMaxs.append(ChannelMax)
            
        MaxCountsPerMicrosecond=np.divide(ExposureTime,ChannelMaxs)
        OptimalExposureTimes=np.multiply(MaxCountsPerMicrosecond,self.TargetCounts)
        '''
        import pandas as pd
        d = {'RowStart': RowStart,'ColumnStart':ColumnStart, 'RowCount': RowCount, 'ColumnCount':ColumnCount,'OptimalExposureTimes':OptimalExposureTimes}
        df = pd.DataFrame(data=d)
              
        

        self.ChannelMapFolder.mkdir(parents=True, exist_ok=True)
        ChannelMapFileName="ChannelMap_"+time.strftime("%Y_%m_%d__%H_%M_%S", time.localtime())+".csv"
        File=Path.joinpath(self.ChannelMapFolder,ChannelMapFileName)
        
        self.ExpTimeArray=OptimalExposureTimes
        
        df.to_csv(File)
        '''
        
        return(OptimalExposureTimes)
        
        
        
        ''''

        NewChStart=[]
        NewChEnd=[]
        ChannelMaxs=[]


        for count,chstart in enumerate(ChannelStarts):
            ChannelProfile=SpatialProfile[ChannelStarts[count]:ChannelEnds[count]]
            ChannelMax=np.amax(ChannelProfile)
            ChannelMaxs.append(ChannelMax)
            ChannelCutoff=ChannelMax*(self.CutoffPercentage/100)
            NewChStart.append(ChannelStarts[count]+next(x[0] for x in enumerate(ChannelProfile) if x[1] > ChannelCutoff))
            NewChannelProfile=SpatialProfile[(NewChStart[count]):ChannelEnds[count]]
            
            print(next(x[0] for x in enumerate(NewChannelProfile) if x[1] < ChannelCutoff)
            NewChEnd.append(NewChStart[count]+next(x[0] for x in enumerate(NewChannelProfile) if x[1] < ChannelCutoff))
                  
        NewRowStart=np.zeros(len(NewChStart))
        NewRowCount=np.ones(len(NewChEnd))*719
        NewColumnCount=np.subtract(NewChEnd,NewChStart)
        NewColumnStart=NewChStart

        d2 = {'RowStart': NewRowStart,'ColumnStart':NewColumnStart, 'RowCount': NewRowCount, 'ColumnCount':NewColumnCount}
        df2 = pd.DataFrame(data=d2)
        print(df2)

        return df2
        '''
    def LoadNewestChannelMap(self):
        FolderLocation1=self.ChannelMapFolder
        ChannelMapList=[]
        for filename in FolderLocation1.rglob('ChannelMap_*.csv'):
            ChannelMapList.append(filename)
        File=sorted(ChannelMapList,reverse=True)[0]
        df=pd.read_csv(File)
        return(File,df)
    
class Reference:
    def __init__(self):
        self.BrightRefFolder=Path('./References/Brights')
        self.DarkRefFolder=Path('./References/Darks')
        
class HyperspecFile:
    def __init__(self):
        self.ImageArray=[]
        self.ChannelMap=[]
        self.BrightRefFolder='./References/Brights/'
        self.DarkRefFolder='./References/Darks/'
        self.ImageDict={}
        self.ExpTimeArray=[]

        
    def WriteCaptureToFile(self,Filename):
        Path(Filename).parent.mkdir(parents=True, exist_ok=True)
        df=self.ChannelMap[1]
        ImageDict={}
        ChannelDict={}
        for count,channel in enumerate(df.iterrows()):
            #print(channel[1]['OptimalExposureTimes'])
            ChannelDict={}
            for index, value in channel[1].items():
                if 'Unnamed' in index:
                    pass
                else:
                    ChannelDict[index]=value
            ChannelDict['RawData']=self.ImageArray[count]
            ImageDict[count]=(ChannelDict)
        with open(Filename, 'wb') as handle:
            pickle.dump(ImageDict, handle, protocol=pickle.HIGHEST_PROTOCOL)
        self.ImageDict=ImageDict
        return(ImageDict)
    
    def LoadLastBright(self):
        FolderLocation1=Path(self.BrightRefFolder)
        BrightsFileList=[]
        for filename in FolderLocation1.rglob('Bright_*.pickle'):
            BrightsFileList.append(filename)
        File=sorted(BrightsFileList,reverse=True)[0]
        with open(File, 'rb') as handle:
            BrightsDict=pickle.load(handle)
        self.ImageDict=BrightsDict
        return(BrightsDict)
    
    def LoadLastDark(self):
        FolderLocation1=Path(self.DarkRefFolder)
        DarksFileList=[]
        for filename in FolderLocation1.rglob('Dark_*.pickle'):
            DarksFileList.append(filename)
        File=sorted(DarksFileList,reverse=True)[0]
        with open(File, 'rb') as handle:
            DarksDict=pickle.load(handle)
        self.ImageDict=DarksDict
        return(DarksDict)
    
    def ReturnRawSpectrum(self,ImageDict=None):
        if ImageDict == None:
            ImageDict=self.ImageDict
        else:
            pass
        ImageArray=[]
        RawSpec=[]
        for count,ChannelDict in enumerate(ImageDict):
            Image=(ImageDict[count]['RawData'])
            #Image=ChannelDict['RawData']
            ImageArray.append(Image)
            RawSpec.append(np.mean(Image, axis=0))
        return(RawSpec)
   
        
class AltoOperator:
    def __init__(self):
        """
        This class acts as the tool box for other oqc components
        """

        # Create a new alto core object
        self.alto_core = AltoCore()

    def load_cartridge(self):
        load_cartridge(self.alto_core)

    def unload_cartridge(self):
        eject_cartridge(self.alto_core)

    def toggle_led_illumination(self, on):
        # when on, output full 1V8
        self.alto_core.turn_on_hsi_illumination(on)

    def toggle_hsi_injection(self,on):
        # will call muxt command to turn on injection gpio and muxe command to turn off
        TEST_MUXT = "MUXT 0x0000000000000000000000000040000000080000000000000000000000000000"
        TEST_MUXE = "MUXE 0x0000000000000000000000000000000004000000000000000000000000000000"
        if on == True:
            try:
                resp = self.alto_core.send_hv_serial(TEST_MUXT)
            except Exception as e:
                raise Exception(e)
        else:
            try:
                resp = self.alto_core.send_hv_serial(TEST_MUXE)
            except Exception as e:
                raise Exception(e)


